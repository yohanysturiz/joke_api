from app import app
import requests_mock

def test_get_chuck_joke(mocker):
    with requests_mock.Mocker() as mock:
        mock.get(
            'https://api.chucknorris.io/jokes/random/',
            json={
                "categories": [],
                "created_at": "2020-01-05 13:42:20.568859",
                "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                "id": "odI7JuFFSeS4Pwj18D0a0w",
                "updated_at": "2020-01-05 13:42:20.568859",
                "url": "https://api.chucknorris.io/jokes/odI7JuFFSeS4Pwj18D0a0w",
                "value": "Allstate won't quote a rate for mayhem like Chuck Norris!"
            }
        )

        client = app.test_client()
        response = client.get("http://localhost:5000/joke/chuck")

        assert response.status_code == 200
        assert response.json == {
            "code": 2000,
            "joke": {
                "categories": [],
                "created_at": "2020-01-05 13:42:20.568859",
                "icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                "id": "odI7JuFFSeS4Pwj18D0a0w",
                "updated_at": "2020-01-05 13:42:20.568859",
                "url": "https://api.chucknorris.io/jokes/odI7JuFFSeS4Pwj18D0a0w",
                "value": "Allstate won't quote a rate for mayhem like Chuck Norris!"
            },
            "status_message": "Success"
        }


def test_get_dad_joke(mocker):
    with requests_mock.Mocker() as mock:
        mock.get(
            'https://icanhazdadjoke.com/',
            json={
                "id": "scaUn3TfNe",
                "joke": "So, I heard this pun about cows, but it’s kinda offensive so I won’t say it. I don’t want there to be any beef between us. ",
                "status": 200
            }
        )

        client = app.test_client()
        response = client.get("http://localhost:5000/joke/dad")

        assert response.status_code == 200
        assert response.json == {
            "code": 2000,
            "joke": {
                "id": "scaUn3TfNe",
                "joke": "So, I heard this pun about cows, but it’s kinda offensive so I won’t say it. I don’t want there to be any beef between us. ",
                "status": 200
            },
            "status_message": "Success"
        }
