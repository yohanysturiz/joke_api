from datetime import datetime

from config import db


class Jokes(db.Model):
    __tablename__ = "jokes"

    id = db.Column(db.Integer, primary_key=True)
    external_id = db.Column(db.String(50))
    content = db.Column(db.String(250), unique=True)
    provider_joke = db.Column(db.String(100), unique=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)

    def get_joke(id):
        joke = Jokes.query.filter_by(id=id).first()

        return joke

    def save(self):
        db.session.add(self)
        db.session.commit()

        return self

    def update(self):
        db.session.commit()

        return self

    def delete(id):
        joke = Jokes.query.filter_by(id=id).delete()
        db.session.commit()

        return joke
