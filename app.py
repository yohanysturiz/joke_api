import os

from flasgger import Swagger
from flask import Flask
from flask_compress import Compress
from dotenv import load_dotenv

from yoyo import read_migrations, get_backend

from config import db
from routes.joke import joke_routes
from routes.math import math_routes

app = Flask(__name__)
app.debug = True

app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv(
    "DATABASE_JOKE", "postgresql://db_user:db_password@db:5432/jokes_db_api"
)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = "secret_key"

compress = Compress()
compress.init_app(app)

app.config["SWAGGER"] = {"title": "API documentation", "uiversion": 3}

swagger = Swagger(app)

app.register_blueprint(joke_routes)
app.register_blueprint(math_routes)

db.init_app(app)

migrations = read_migrations('migrations')

backend = get_backend('postgresql://db_user:db_password@db:5432/jokes_db_api')
with backend.lock():
    backend.apply_migrations(backend.to_apply(migrations))

if __name__ == "__main__":
    load_dotenv()
    app.run(debug=True)
