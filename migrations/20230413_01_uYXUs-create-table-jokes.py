"""
create table jokes
"""

from yoyo import step

__depends__ = {}

steps = [
    step(
        """
        CREATE SEQUENCE jokes_id_seq;
        CREATE TABLE jokes (
            id INTEGER PRIMARY KEY DEFAULT nextval('jokes_id_seq'),
            external_id VARCHAR(150),
            content VARCHAR(250),
            provider_joke VARCHAR(100),
            created_at TIMESTAMP
        );
    """
    )
]
