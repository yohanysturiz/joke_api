import numpy as np


def least_common_multiple(numbers):
    result = np.lcm.reduce(numbers)
    return result