from models.jokes import Jokes


def save_joke(external_id, content, provider_joke):
    joke = Jokes(
        external_id=external_id,
        content=content,
        provider_joke=provider_joke
    )
    joke.save()

    return joke


def update_joke(joke_id, external_id, content, provider_joke):
    joke = Jokes.get_joke(joke_id)
    if joke:
        joke.external_id = (
            external_id if external_id is not None else joke.external_id
        )
        joke.content = content if content is not None else joke.content
        joke.provider_joke = (
            provider_joke if provider_joke is not None else joke.provider_joke
        )
        joke.update()
    else:
        return None

    return joke


def delete(joke_id: int):
    joke = Jokes.delete(joke_id)

    return joke
