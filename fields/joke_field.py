from pydantic import BaseModel, Field


class JokeFields(BaseModel):
    external_id: str = Field(..., max_length=150)
    content: str = Field(..., max_length=250)
    provider_joke: str = Field(..., max_length=100)


class JokeFieldsUpdate(BaseModel):
    external_id: str = Field(default=None, max_length=150)
    content: str = Field(default=None, max_length=250)
    provider_joke: str = Field(default=None, max_length=100)
