FROM python:3.11

WORKDIR /app

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

ENV FLASK_APP=app.py
ENV DATABASE_JOKE=postgresql://db_user:db_password@db:5432/jokes_db_api

EXPOSE 5000

CMD ["flask", "run", "--host", "0.0.0.0"]