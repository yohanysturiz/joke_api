from typing import Optional

import requests


class ChuckServiceApi:
    def __init__(self, search_param: Optional[str] = None) -> None:
        self.search_param = search_param

    @staticmethod
    def get_joke() -> dict:
        response = requests.get(
            "https://api.chucknorris.io/jokes/random/",
            headers={"Accept": "application/json"},
        )
        joke = response.json()
        return joke
