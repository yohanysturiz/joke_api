from flasgger import swag_from
from flask import Blueprint, request

from constants import MATH_GET_INCREMENT_ERROR, MATH_GET_LCM_SUCCESS
from response_provider import MathProviderResponse
from services.math_service import least_common_multiple

math_routes = Blueprint("math_routes", __name__)


@math_routes.route("/math/lcm", methods=["GET"])
@swag_from("../swagger/get_math_lcm.yml")
def get_lcm() -> dict:
    numbers = request.args.get("numbers")
    number_list = [int(n) for n in numbers.split(",")]

    result = least_common_multiple(number_list)

    response = MathProviderResponse(
        code=MATH_GET_LCM_SUCCESS, result=int(result)
    ).message_success()

    return response


@math_routes.route("/math/increment", methods=["GET"])
@swag_from("../swagger/get_math_increment.yml")
def get_increment() -> dict:
    number = request.args.get("number", type=int)
    if number is None:
        return MathProviderResponse(
            code=MATH_GET_INCREMENT_ERROR,
        ).message_error()

    response = MathProviderResponse(
        code=MATH_GET_LCM_SUCCESS, result=int(number + 1)
    ).message_success()

    return response
