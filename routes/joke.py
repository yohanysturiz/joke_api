from flasgger import swag_from
from flask import Blueprint, request
from flask_pydantic import validate

from constants import (CHUCK_JOKE_API, DAD_JOKE_API, JOKE_CREATE_SUCCESS,
                       JOKE_DELETE_SUCCESS, JOKE_NOT_EXIST,
                       JOKE_PROVIDER_NOT_FOUND, JOKE_PROVIDER_TYPES,
                       JOKE_RANDOM_SUCCESS, JOKE_UPDATE_SUCCESS)
from fields.joke_field import JokeFields, JokeFieldsUpdate
from response_provider import JokeProviderResponse
from services.joke_service import delete, save_joke, update_joke
from utils.chuck_service_api import ChuckServiceApi
from utils.dad_service_api import DadServiceApi
import random

joke_routes = Blueprint("joke_routes", __name__)


@joke_routes.route("/")
def hello_world():
    return "¡Hola, mundo!"


@joke_routes.route("/joke", methods=["GET"])
def get_random_joke() -> dict:
    jokes_providers = [
        DadServiceApi.get_joke(),
        ChuckServiceApi.get_joke()
    ]

    joke_random = random.choice(jokes_providers)

    response = JokeProviderResponse(
        code=JOKE_RANDOM_SUCCESS, joke=joke_random
    ).message_success()

    return response


@joke_routes.route("/joke/<joke_provider>", methods=["GET"])
@swag_from("../swagger/get_joke.yml")
def get_joke(joke_provider: str) -> dict:
    provider = joke_provider.upper()

    if provider in JOKE_PROVIDER_TYPES:
        if provider == CHUCK_JOKE_API:
            joke = ChuckServiceApi.get_joke()
        if provider == DAD_JOKE_API:
            joke = DadServiceApi.get_joke()

        response = JokeProviderResponse(
            code=JOKE_RANDOM_SUCCESS, joke=joke
        ).message_success()

    else:
        response = JokeProviderResponse(
            code=JOKE_PROVIDER_NOT_FOUND,
        ).message_error()

    return response


@joke_routes.route("/joke", methods=["POST"])
@validate(body=JokeFields)
@swag_from("../swagger/post_joke.yml")
def post_joke() -> dict:
    external_id = request.json.get("external_id", None)
    content = request.json.get("content", None)
    provider_joke = request.json.get("provider_joke", None)

    joke = save_joke(external_id, content, provider_joke)

    response = JokeProviderResponse(
        code=JOKE_CREATE_SUCCESS, joke={"id": joke.id, "content": joke.content}
    ).message_success()

    return response


@joke_routes.route("/joke/<joke_id>", methods=["PUT"])
@validate(body=JokeFieldsUpdate)
@swag_from("../swagger/put_joke.yml")
def put_joke(joke_id: int) -> dict:
    external_id = request.json.get("external_id", None)
    content = request.json.get("content", None)
    provider_joke = request.json.get("provider_joke", None)

    joke = update_joke(joke_id, external_id, content, provider_joke)

    if joke is None:
        return JokeProviderResponse(
            code=JOKE_NOT_EXIST,
        ).message_error()

    response = JokeProviderResponse(
        code=JOKE_UPDATE_SUCCESS, joke={"id": joke.id, "content": joke.content}
    ).message_success()

    return response


@joke_routes.route("/joke/<joke_id>", methods=["DELETE"])
@swag_from("../swagger/delete_joke.yml")
def delete_joke(joke_id: int) -> dict:
    joke = delete(joke_id)

    if not joke:
        return JokeProviderResponse(
            code=JOKE_NOT_EXIST,
        ).message_error()

    response = JokeProviderResponse(code=JOKE_DELETE_SUCCESS).message_success()

    return response
