from flask import make_response

from constants import JOKE_RESPONSE_CODES, MATH_RESPONSE_CODES


class JokeProviderResponse:
    def __init__(self, code: int, joke: dict = None):
        self.code = code
        self.status_message = JOKE_RESPONSE_CODES[code]
        self.joke = joke

    def message_error(self):
        return make_response(
            {
                "code": self.code,
                "status_message": self.status_message,
                "joke": self.joke,
            },
            404,
        )

    def message_success(self):
        return make_response(
            {
                "code": self.code,
                "status_message": self.status_message,
                "joke": self.joke,
            },
            200,
        )


class MathProviderResponse:
    def __init__(self, code: int, result: int):
        self.code = code
        self.status_message = MATH_RESPONSE_CODES[code]
        self.result = result

    def message_error(self):
        return make_response(
            {
                "code": self.code,
                "status_message": self.status_message,
                "result": self.result,
            },
            404,
        )

    def message_success(self):
        return make_response(
            {
                "code": self.code,
                "status_message": self.status_message,
                "result": self.result,
            },
            200,
        )
